// sp_lab1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#define DIV 1024
#define WIDTH 7


int _tmain(int argc, _TCHAR* argv[])
{
	MEMORYSTATUSEX statex;
	LPVOID lpMsg;
	DWORD dw;
	setlocale(LC_ALL, "Rus");

	if(argc>1)
	{
		if(wcscmp(argv[1],L"-e")==0)
		{
			LPTSTR pszBuf=NULL;
			pszBuf = (LPTSTR)LocalAlloc(LPTR,8000000000);			
			if( pszBuf == NULL )
			{	
				dw = GetLastError();
				printf(("LocalAlloc failed (%d)\n"), GetLastError());
				FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					dw,
					MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),
					(LPTSTR)&lpMsg,
					0,
					NULL);
				printf(("%s"),lpMsg);
				puts(""); 
				return 0;
			}
			LocalFree(pszBuf);
		}
		else if(wcscmp(argv[1],L"-s")==0)
		{
			SYSTEM_INFO siSysInfo;	
			GetSystemInfo(&siSysInfo); 		
			statex.dwLength = sizeof (statex);
			GlobalMemoryStatusEx (&statex);

			puts("Information about system");
			printf("Hardware information: \n"); 
			printf("OEM ID: %u\n", siSysInfo.dwOemId); 
			printf("Number of processors: %u\n", 
				siSysInfo.dwNumberOfProcessors); 
			printf("Page size: %u\n", siSysInfo.dwPageSize); 
			printf("Processor type: %u\n", siSysInfo.dwProcessorType); 
			printf("Minimum application address: %lx\n", 
				siSysInfo.lpMinimumApplicationAddress); 
			printf("Maximum application address: %lx\n", 
				siSysInfo.lpMaximumApplicationAddress); 
			printf("Active processor mask: %u\n", 
				siSysInfo.dwActiveProcessorMask);
			puts("");
			puts("Information about memory");
			printf (("There is  %*ld percent of memory in use.\n"),
				WIDTH, statex.dwMemoryLoad);
			printf (("There are %*I64d total KB of physical memory.\n"),
				WIDTH, statex.ullTotalPhys/DIV);
			printf (("There are %*I64d free  KB of physical memory.\n"),
				WIDTH, statex.ullAvailPhys/DIV);
			printf (("There are %*I64d total KB of paging file.\n"),
				WIDTH, statex.ullTotalPageFile/DIV);
			printf (("There are %*I64d free  KB of paging file.\n"),
				WIDTH, statex.ullAvailPageFile/DIV);
			printf (("There are %*I64d total KB of virtual memory.\n"),
				WIDTH, statex.ullTotalVirtual/DIV);
			printf (("There are %*I64d free  KB of virtual memory.\n"),
				WIDTH, statex.ullAvailVirtual/DIV);
			printf (("There are %*I64d free  KB of extended memory.\n"),
				WIDTH, statex.ullAvailExtendedVirtual/DIV);
		}
		else
	{
		puts("������� ���������� ��������� ��������� ������!");
	}
	}
		else
	{
		puts("������� ��������� ��������� ������!");
	}
	return 0;	
}	
