// sp_lab2.cpp: ���������� ����� ����� ��� ����������� ����������.
//
//TODO!!! ����� ����, ������ ������ � �����
//

#include "stdafx.h"

int Split(wchar_t*a,wchar_t**b,wchar_t*c);
bool checkPath(wchar_t* curDir,wchar_t* src);
void ls(wchar_t*);
void cd(wchar_t*,wchar_t**);
void mkdir(wchar_t*,wchar_t**);
void rmdir(wchar_t*,wchar_t**);
void rmfile(wchar_t*,wchar_t**);
void cp(wchar_t*,wchar_t**);
void finfo(wchar_t**);

wchar_t startDir[100];

int main(int argc, char* argv[])
{
	wchar_t commands[7][7] = {L"cd",L"ls",L"mkdir",L"rmdir",L"rmfile",L"cp", L"finfo"};
	wchar_t descriptions[7][70] = {L"(path) - change directory",L"(path) - print all files and directoryes",L"(folder) - create new folder",
		L"(folder) - delete folder",L"(file) - delete file",L"(source,destination) - copy from source to destination",
		L"(file) - print info about file"};
	wchar_t command[100];
	wchar_t* arr[10];
	wchar_t curDir[100],tmpDir[100],tmpPath[100];
	int count;
	wchar_t *direction;
	int idCmd;
	SetCurrentDirectory(L"WorkingDir");
	GetCurrentDirectory(100,curDir);
	wcscpy(startDir, curDir);
	_putws(L"Commands:");
	for (int i = 0; i < 7; i++)
	{
		wprintf(L"%s %s\n",commands[i],descriptions[i]);
	}
	_putws(L"");
	for(;;)
	{		
		GetCurrentDirectory(100,curDir);
		wprintf(L"%s: ",curDir);
		_getws(command);
		count = Split(command,arr,L" ");
		idCmd=-1;
		for (int i = 0; i < 7; i++)
		{
			wchar_t *a = commands[i];
			if (wcscmp(command,commands[i])==0)
			{
				idCmd = i;
				break;
			}
		}
		switch (idCmd)
		{
		case 0:
			if(count>2)
				_putws(L"�������� ���������o ����������");
			else if(count==2)
			{
				cd(curDir,arr);
			}
			else
			{
				SetCurrentDirectory(startDir);
			}
			break;
		case 1:
			if(count!=2)
				_putws(L"�������� ��������� �����������");
			else 			
			{
				Split(command,arr,L" ");
				ls(arr[1]);
			}
			break;
		case 2:
			if(count!=2)
				_putws(L"�������� ��������� �����������");
			else
			{	
				mkdir(curDir,arr);
			}
			break;
		case 3:
			if(count!=2)
				_putws(L"�������� ��������� �����������");
			else
			{
				rmdir(curDir,arr);
			}
			break;			
		case 4:			
			if(count!=2)
				_putws(L"�������� ��������� �����������");
			else
			{
				rmfile(curDir,arr);
			}
			break;			
		case 5:			
			if(count!=3)
				_putws(L"�������� ��������� �����������");
			else
			{
				cp(curDir,arr);
			}
			break;			
		case 6:			
			if(count!=2)
				_putws(L"�������� ��������� �����������");
			else
			{
				finfo(arr);
			}
			break;
		default:
			_putws(L"������ �������� �� ����������!");
			break;
		}
	}
	return 0;
}

int Split(wchar_t* str, wchar_t**arr,wchar_t* splitter)
{
	int count=0;
	wchar_t* dst;
	dst = wcstok(str,splitter);
	while (dst != NULL)                         
	{
		arr[count] = dst;
		dst = wcstok (NULL, splitter);
		count++;
	}
	return count;
}

bool checkPath(wchar_t* curDir,wchar_t* src)
{
	bool ret;
	wchar_t tmp[100];
	wchar_t* arr[10];
	wchar_t separator[10];
	wcscpy(tmp,curDir);
	wcscpy(tmp,L"");
	if(wcsstr(src,L"\\")!=NULL||wcsstr(src,L"/")!=NULL)
	{
		if(wcsstr(src,L"\\")!=NULL)
		{
			wcscpy(separator,L"\\");
		}
		else
		{
			wcscpy(separator,L"/");
		}
	}
	for (int i = 0; i < Split(src,arr,separator)-1; i++)
	{
		wcscat(tmp,arr[i]);
		wcscat(tmp,separator);
	}
	SetCurrentDirectory(tmp);
	GetCurrentDirectory(100,tmp);
	if(wcsstr(tmp,startDir)==NULL)
	{
		ret = false;
	}
	else
	{
		ret = true;
	}	
	SetCurrentDirectory(curDir);
	return ret;
}

void ls(wchar_t* dir)
{
	wprintf(L"K�������: \n");
	WIN32_FIND_DATA fd;
	LARGE_INTEGER filesize;
	SYSTEMTIME sTime;
	HANDLE fp;
	wcscat(dir,L"/*");
	fp = FindFirstFile(dir,&fd);
	if(fp==INVALID_HANDLE_VALUE)
	{
		exit(0);
	}	
	do
	{
		_putws(L"____________________________________________");
		if(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			_tprintf(L"%s   <DIR>\n",fd.cFileName);
		else
			_tprintf(L"%s   <FILE>\n",fd.cFileName);

		filesize.LowPart = fd.nFileSizeLow;
		filesize.HighPart = fd.nFileSizeHigh;
		FileTimeToSystemTime(&fd.ftCreationTime,&sTime);
		wprintf(L"Size of file: %ld byte\n",filesize);
		wprintf(L"Creation time: %ld.%ld.%ld  %ld:%ld:%ld\n",sTime.wDay,sTime.wMonth,sTime.wYear,sTime.wHour,sTime.wMinute,sTime.wSecond);		
	}
	while (FindNextFile(fp,&fd)!=0);
	FindClose(fp);
}

void cd(wchar_t* curDir, wchar_t** arr)
{
	wchar_t tmpDir[100];
	wcscpy(tmpDir,curDir);
	if(!SetCurrentDirectory(arr[1]))
		_putws(L"������� ���������� ����!");
	else 
	{
		GetCurrentDirectory(100,curDir);
		if(wcsstr(curDir,startDir)==NULL)
		{
			_putws(L"�������� � �������!");
			wcscpy(curDir,tmpDir);						
			SetCurrentDirectory(curDir);
		}
	}
}

void mkdir(wchar_t* curDir, wchar_t** arr)
{
	wchar_t tmpDir[100],tmpPath[100];
	wcscpy(tmpDir,curDir);
	wcscpy(tmpPath,arr[1]);
	if(!checkPath(curDir,arr[1]))
	{
		_putws(L"�������� � �������!");
		wcscpy(curDir,tmpDir);						
		SetCurrentDirectory(curDir);
	}
	else
		if(!CreateDirectory(tmpPath,0))
			_putws(L"�� ������� ������� �����!");
		else
			_putws(L"������ ������� ��������!");
}

void rmdir(wchar_t* curDir, wchar_t** arr)
{
	wchar_t tmpDir[100],tmpPath[100];
	wcscpy(tmpDir,curDir);
	wcscpy(tmpPath,arr[1]);
	if(!checkPath(curDir,arr[1]))
	{
		_putws(L"�������� � �������!");
		wcscpy(curDir,tmpDir);						
		SetCurrentDirectory(curDir);
	}
	else
		if(!RemoveDirectory(tmpPath))
		{
			_putws(L"�� ������� ������� �����!");
		}
		else
			_putws(L"������ ������� �������!");
}

void rmfile(wchar_t* curDir, wchar_t** arr)
{
	wchar_t tmpDir[100],tmpPath[100];
	wcscpy(tmpDir,curDir);
	wcscpy(tmpPath,arr[1]);
	if(!checkPath(curDir,arr[1]))
	{
		_putws(L"�������� � �������!");
		wcscpy(curDir,tmpDir);						
		SetCurrentDirectory(curDir);
	}
	else
		if(!DeleteFile(tmpPath))
		{
			_putws(L"�� ������� ������� ����!");
		}
		else
			_putws(L"���� ������� ������!");				
}

void cp(wchar_t* curDir,wchar_t** arr)
{
	wchar_t tmpDir[100],tmpPath[100];
	wcscpy(tmpDir,arr[1]);
	wcscpy(tmpPath,arr[2]);
	if(!checkPath(curDir,arr[1])||!checkPath(curDir,arr[2]))
	{
		_putws(L"�������� � �������!");
	}
	else
		if(!CopyFile(tmpDir,tmpPath,true))
		{
			_putws(L"�� ������� ����������� ����!");
		}
		else
			_putws(L"���� ������� ����������!");		
}

void finfo(wchar_t** arr)
{
	SYSTEMTIME sTime;
	LARGE_INTEGER filesize;
	int locCount;
	HANDLE hFile = CreateFile(arr[1],GENERIC_READ, FILE_SHARE_READ,NULL,OPEN_EXISTING,0,0);
	BY_HANDLE_FILE_INFORMATION info;
	GetFileInformationByHandle(hFile,&info);
	locCount = Split(arr[1],arr,L"\\");
	filesize.LowPart = info.nFileSizeLow;
	filesize.HighPart = info.nFileSizeHigh;
	wprintf(L"Size of file: %ld byte\n",filesize);
	FileTimeToSystemTime(&info.ftCreationTime,&sTime);
	wprintf(L"Creation time: %ld.%ld.%ld  %ld:%ld:%ld\n",sTime.wDay,sTime.wMonth,sTime.wYear,sTime.wHour,sTime.wMinute,sTime.wSecond);
	FileTimeToSystemTime(&info.ftLastAccessTime,&sTime);
	wprintf(L"Last access time: %ld.%ld.%ld  %ld:%ld:%ld\n",sTime.wDay,sTime.wMonth,sTime.wYear,sTime.wHour,sTime.wMinute,sTime.wSecond);
	FileTimeToSystemTime(&info.ftLastWriteTime,&sTime);
	wprintf(L"Last write time: %ld.%ld.%ld  %ld:%ld:%ld\n",sTime.wDay,sTime.wMonth,sTime.wYear,sTime.wHour,sTime.wMinute,sTime.wSecond);
	wprintf(L"Number of links: %ld\n",info.nNumberOfLinks);

}