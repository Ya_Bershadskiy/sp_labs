<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 20.03.2017
 * Time: 21:00
 */

include("classes/Functions.php");
include("classes/MyDB.php");
include("classes/Ses.php");

global $mybd;
session_start();
$is_user = false;
$login = GetPar('login');
$password = GetPar('password');

$mydb = new MyDB('127.0.0.1', 'root', '', 'yarikstore');
$ses = new Ses();

//Разобраться с подключением к БД
if (!$mydb->connect()) {
    echo "Error connect";
    exit;
}
if (isset($_POST['logIn']))
    if (!$ses->tryAuthUser($login, $password)) {
        echo "<h2>Неверный логин или пароль</h2>";
    }

if ($ses->checkAuthUser()) {
    header("Location: cmsadmin/index.php");
}

$mydb->close();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/form.css">
    <title>Вход</title>
</head>
<body>
    <div class="mainBlock">
        <form action="" method="POST">
            <p>Логин</p>
            <input name="login" type="text" required>
            <p>Пароль</p>
            <input name="password" minlength="6" type="password" required><br>
            <input type="submit" value="Войти" name="logIn">
        </form>
        <div id="hrefBut">
            <a href="inc/register.php">Зарегистрироваться</a>
        </div>
    </div>
</body>
</html>

