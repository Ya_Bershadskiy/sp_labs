<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 23.03.2017
 * Time: 20:40
 */
include("../classes/Functions.php");
include("../classes/MyDB.php");
include("../classes/Ses.php");

$mydb = new MyDB('127.0.0.1', 'root', '', 'yarikstore');
$ses = new Ses();

//Разобраться с подключением к БД
if (!$mydb->connect()) {
    echo "Error connect";
    exit;
}
if ($ses->checkAuthUser()) {
    header("Location: cmsadmin/index.php");
}
$regName = GetPar('regName');
$regLog = GetPar('regLog');
$regPas = GetPar('regPas');

if (isset($_POST['registr'])) {
    $mdPas = md5($regPas);
    $q = "INSERT INTO users(access,name,login,password,active) VALUES(0,'$regName','$regLog','$mdPas',1)";
    $mydb->query($q);
    header("Location: ../index.php");
    UnsetPar('regLog');
    UnsetPar('regPas');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/register.css">
    <title>Регистрация</title>
</head>
<body>
<div class="mainBlock">
    <form action="" method="post">
        <table >
            <tr>
                <td><p>Имя:</p></td>
                <td><input type="text" name="regName" required></td>
            </tr>
            <tr>
                <td><p>Логин:</p></td>
                <td><input type="text" name="regLog" required></td>
            </tr>
            <tr>
                <td><p>Пароль:</p></td>
                <td><input type="password" name="regPas" minlength="6" required></td>
            </tr>
        </table>
        <input type="submit" value="Зарегестрироваться" name="registr">
    </form>
</div>
</body>
</html>


