<?php
define("TABLE_USERS", "users");
define("TABLE_SETTINGS", "settings");
define("TABLE_PRODUCTS", "products");
define("TABLE_USER_AUTH", "auth");
define("PATH_TO_IMAGES", "../cmsadmin/img/");
define("TABLE_COMMENTS", "comments");
define("TABLE_CATEGORIES", "categories");

function GetPar($name, $def = "")
{
    $val = $def;
    if (isset($_POST[$name]))
        $val = $_POST[$name];
    elseif (isset($_GET[$name]))
        $val = $_GET[$name];
    return $val;
}

function UnsetPar($name)
{
    unset($_POST[$name]);
    unset($_GET[$name]);
}

function SaveFile($img)
{
    if (isset($_FILES[$img]) && $_FILES[$img]['name'] != "") {
        if ($_FILES[$img]['tmp_name'] != "" && file_exists($_FILES[$img]['tmp_name'])) {
            copy($_FILES[$img]['tmp_name'], PATH_TO_IMAGES . $_FILES[$img]['name']);
            unlink($_FILES[$img]['tmp_name']);
        }
    }
}

?>