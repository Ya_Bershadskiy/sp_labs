<?php

/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 25.03.2017
 * Time: 10:43
 */
class Ses
{
    private $SESID;
    private $ses_path;
    private $is_auth;
    private $user_id;
    private $user_name;

    public function __construct($ses_path_val = "", $use_user = false)
    {
        $this->ses_path = $ses_path_val;
        if ($this->ses_path != "") {
            session_save_path($this->ses_path);
        }
        @session_start();
        $this->SESID = session_id();
        $this->user_id = 0;
    }

    public function getSessionId()
    {
        return $this->SESID;
    }

    public function tryAuthUser($login, $pass)
    {
        global $mydb;
        $q = "SELECT * FROM " . TABLE_USERS . " WHERE login='" . addslashes($login) . "' AND password='" . md5(addslashes($pass)) . "'";
        $res = $mydb->query($q);
        if (count($res) > 0) {
            if ($res[0]['active'] > 0) {
                //Разрешить вход
                $q = "INSERT INTO " . TABLE_USER_AUTH . " VALUES ('" . addslashes($this->getSessionId()) . "','" . md5(addslashes($pass)) . "','" . addslashes($login) . "')";

                $mydb->query($q);
                $this->user_id = $res[0]['id'];
                return true;
            } else {
                echo "<h2>Ваш аккаунт недоступен</h2>";
                return true;
            }
        }
        return false;
    }

    public function checkAuthUser()
    {
        global $mydb;
        $q = "SELECT u1.* FROM " . TABLE_USERS . " u1 INNER JOIN " . TABLE_USER_AUTH . " a1 ON
        a1.ses_id = '" . addslashes($this->SESID) . "' AND a1.login=u1.login AND a1.password = u1.password";
        $res = $mydb->query($q);
        if (count($res) > 0) {
            $this->user_id = $res[0]['id'];
            $this->user_name = $res[0]['name'];
            return true;
        }
        return false;
    }

    public function logOut()
    {
        global $mydb;
        $q = "DELETE FROM " . TABLE_USER_AUTH . " WHERE ses_id = '" . $this->SESID . "'";
        $mydb->query($q);
    }

    public function getAccess()
    {
        $this->checkAuthUser();
        global $mydb;
        $q = "SELECT access FROM " . TABLE_USERS . " WHERE id = " . $this->user_id;
        $access = $mydb->query($q);
        return $access[0]['access'];
    }

    public function getCurName()
    {
        $this->checkAuthUser();
        return $this->user_name;
    }
}