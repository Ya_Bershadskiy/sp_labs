<?php

/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 20.03.2017
 * Time: 21:17
 */
class MyDB
{
    private $chost;
    private $clog;
    private $cpass;
    private $cdb;
    private $db;

    public function __construct($h, $l, $p, $d)
    {
        $this->chost = $h;
        $this->clog = $l;
        $this->cpass = $p;
        $this->cdb = $d;
    }

    public function is_connected()
    {
        return ($this->db!=null);
    }

    public function connect()
    {
        $this->db=new MySqli($this->chost,$this->clog,$this->cpass,$this->cdb);
        if (!$this->db)
        {
            die("Error connect");
        }
        return $this->is_connected();
    }

    public function exec($q){
        if(!$this->db->query($q))
            return false;
        return true;
    }

    public function query($q)
    {
        $dat = Array();
        if ($res = $this->db->query($q))
        {
            if(!is_bool($res)) {
                while ($row = $res->fetch_assoc()) {
                    $dat[] = $row;
                }
            }
        }
        return $dat;
    }

    public function close()
    {
        $this->db->close();
    }
}