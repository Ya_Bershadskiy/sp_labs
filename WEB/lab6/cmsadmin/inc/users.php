<link rel="stylesheet" href="css/users.css">
<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 25.03.2017
 * Time: 15:54
 */

if (isset($_POST['ban'])) {
    $q="UPDATE ".TABLE_USERS." SET active=0 WHERE id=".$_POST['banId'];
    $mydb->query($q);
}
if (isset($_POST['unBan'])) {
    $q="UPDATE ".TABLE_USERS." SET active=1 WHERE id=".$_POST['banId'];
    $mydb->query($q);
}
if (isset($_POST['delete'])) {
    $q="DELETE FROM ".TABLE_USERS." WHERE id=".$_POST['banId'];
    $mydb->query($q);
}

$q = "SELECT * FROM " . TABLE_USERS;
$users = $mydb->query($q);

echo "<div class='workArea'>";
foreach ($users as $user) {
    $access = ($user['access'] == 0) ? 'user' : 'admin';
    $status = ($user['active'] == 1) ? 'active' : 'deactivated';
    echo
        "<div class='List'>
            <h4>ID: " . $user['id'] . "</h4>
            <p><b>Name: " . $user['name'] . "</b><p>
            <p>Login: " . $user['login'] . "<p>
            <p>Password: " . $user['password'] . "<p>
            <p>Access: " . $access . "<p>
            <p>Status: " . $status . "<p>
            <form action='' method='post'>
                <input type='hidden' value='" . $user['id'] . "' name='banId'>
                <input type='submit' value='Active' name='unBan'>
                <input type='submit' value='Deactive' name='ban'>
                <input type='submit' value='Delete' name='delete'>
            </form>
        </div>";
}
echo "</div>";
?>

