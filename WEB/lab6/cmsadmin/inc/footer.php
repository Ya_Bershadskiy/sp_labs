<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 25.03.2017
 * Time: 14:35
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/footer.css">
    <title>Document</title>
</head>
<body>
<div id="footer">
    <div id="text">
        <table >
            <tr>
                <td class="border_td"></td>
                <td>Contacts:</td>
            </tr>
            <tr>
                <td class="border_td">CMS by Bershadskiy Yaroslav</td>
                <td>Tel: 0984659380:</td>
            </tr>
            <tr>
                <td class="border_td"></td>
                <td>Emal: ya.bershadskiy@hotmail.com:</td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
