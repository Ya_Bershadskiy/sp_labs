<link rel="stylesheet" href="css/settings.css">
<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 25.03.2017
 * Time: 21:33
 */
$mydb = new MyDB('127.0.0.1', 'root', '', 'yarikstore');
$mydb->connect();

if (isset($_POST['choose'])) {
    $q = "UPDATE ".TABLE_SETTINGS." SET showComment=".GetPar('showComment').", showDecription=".GetPar('showDecription');
    $mydb->query($q);
}
?>
<div class='workArea'>
<form action="" method="post">
    <label>Показывать комментарии <br>
        <input type="radio" name="showComment" value="1" checked>Да<br>
        <input type="radio" name="showComment" value="0">Нет<br></label>
    <label>Показывать описание товара <br>
        <input type="radio" name="showDecription" value="1" checked>Да<br>
        <input type="radio" name="showDecription" value="0">Нет<br></label>
    <input type="submit" value="Подтвердить выбор" name="choose">
</form>
</div>