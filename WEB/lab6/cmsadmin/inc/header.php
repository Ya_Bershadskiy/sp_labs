<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 25.03.2017
 * Time: 14:34
 */
$mydb = new MyDB('127.0.0.1', 'root', '', 'yarikstore');
$ses = new Ses();
$mydb->connect();

$isAdmin = ($ses->getAccess() == 1);

echo '<div id="menu">
<table >
        <tr>
            <td>
            <a href="?action=productList">Товары</a>
            </td>
        </tr>';
if ($isAdmin) {
    echo '
    <tr>
        <td>
            <a href="?action=userList">Пользователи</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="?action=comments">Управление отзывами</a>
        </td>
     </tr>
    <tr>
        <td>
            <a href="?action=addCategory">Добавить категорию</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="?action=addProduct">Добавить товар</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="?action=chngProduct">Изменить товар</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="?action=delProduct">Удалить товар</a>
        </td>
    </tr>
    <tr>
        <td>
        <a href="?action=contentManag">Управление контентом</a>
        </td>
    </tr>';
}
echo '
    <tr>
        <td>
       <a href="?action=logOut">Выйти</a>
        </td>
    </tr>
    </table>
</div>';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/header.css">
    <title>Управление</title>
</head>
<body>

</body>
</html>