<?php

include("../classes/Functions.php");
include("../classes/MyDB.php");
include("../classes/Ses.php");

$mydb = new MyDB('127.0.0.1', 'root', '', 'yarikstore');
$ses = new Ses();
$mydb->connect();

if (!$mydb->is_connected()) {
    echo "Ошибка(1)";
    exit;
}

$mode = "list";
$action = "";
$action = GetPar('action');

if ($action == 'logOut') {
    $ses->logOut();
}

if (!$ses->checkAuthUser()) {
    header("Location: ../index.php");
}

switch ($action) {
    case "userList":
        $mode = "userList";
        break;
    case "productList":
        $mode = "productList";
        break;
    case "comments":
        $mode = "comments";
        break;
    case "addProduct":
        $mode = "addProduct";
        break;
    case "delProduct":
        $mode = "delProduct";
        break;
    case "chngProduct":
        $mode = "chngProduct";
        break;
    case "addCategory":
        $mode = "addCategory";
        break;
    case "contentManag":
        $mode = "contentManag";
        break;
    default:
        header("Location:?action=productList");
        break;
}

include "inc/header.php";

switch ($mode) {
    case "userList":
        include("inc/users.php");
        break;
    case "productList":
        include("inc/product.php");
        break;
    case "comments":
        include("inc/comments.php");
        break;
    case "addProduct":
        include("inc/addProduct.php");
        break;
    case "delProduct":
        include("inc/delProduct.php");
        break;
    case "chngProduct":
        include("inc/chngProduct.php");
        break;
    case "addCategory":
        include("inc/addCategory.php");
        break;
    case "contentManag":
        include("inc/contentManag.php");
        break;
}

include "inc/footer.php";