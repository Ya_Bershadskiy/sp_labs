﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    public class Convertor : INotifyPropertyChanged
    {
        private double first;
        private double second;
        private int a;
        private int b;

       
        public double First
        {
            get { return first; }
            set
            {
                if (first != value)
                {
                    first = value;
                    OnPropertyChanged("First");
                }
            }
        }

        public double Second
        {
            get { return second; }
            set
            {
                if (second != value)
                {
                    second = value;
                    OnPropertyChanged("Second");
                }
            }
        }

        public int A
        {
            get { return a; }
            set
            {
                if (a != value)
                {
                    a = value;
                    OnPropertyChanged("A");
                }
            }
        }

        public int B
        {
            get { return b; }
            set
            {
                if (b != value)
                {
                    b = value;
                    OnPropertyChanged("B");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    public sealed partial class HubPage1 : Page
    {

        public HubPage1()
        {
            this.InitializeComponent();
        }

        private void Convert1(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            double tmp=0;
            Convertor conv = sec1.DataContext as Convertor;
            switch (conv.A)
            {
                case 0:
                    tmp = conv.First / 1000;
                    break;
                case 1:
                    tmp = conv.First / 100;
                    break;
                case 2:
                    tmp = conv.First;
                    break;
                case 3:
                    tmp = conv.First * 1000;
                    break;
                default:
                    break;
            }
            switch(conv.B)
            {
                case 0:
                    conv.Second = tmp* 3.28084;
                    break;
                case 1:
                    conv.Second = tmp * 39.3701;
                    break;
                case 2:
                    conv.Second = tmp * 1.09361;
                    break;
                case 3:
                    conv.Second = tmp* 0.000539957;
                    break;
                default:
                    break;
            }
        }

        private void Convert2(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            double tmp = 0;
            Convertor conv1 = sec2.DataContext as Convertor;
            switch (conv1.A)
            {
                case 0:
                    tmp = conv1.First/1000;
                    break;
                case 1:
                    tmp = conv1.First;
                    break;
                case 2:
                    tmp = conv1.First * 100;
                    break;
                case 3:
                    tmp = conv1.First * 1000;
                    break;
                default:
                    break;
            }
            switch (conv1.B)
            {
                case 0:
                    conv1.Second = tmp * 2.20462;
                    break;
                case 1:
                    conv1.Second = tmp * 35.274;
                    break;
                default:
                    break;
            }
        }

        private void Convert3(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            double tmp = 0;
            Convertor conv2 = sec3.DataContext as Convertor;
            switch (conv2.A)
            {
                case 0:
                    tmp = conv2.First * 1000;
                    break;
                case 1:
                    tmp = conv2.First;
                    break;
                case 2:
                    tmp = conv2.First/1000;
                    break;                
                default:
                    break;
            }
            switch (conv2.B)
            {
                case 0:
                    conv2.Second = tmp * 2.11338;
                    break;
                case 1:
                    conv2.Second = tmp * 0.264172;
                    break;
                default:
                    break;
            }
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
