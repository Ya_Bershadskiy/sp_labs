// sp_lab5.2.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

#define MAX_FILE_POINTER 10
#define THREADCOUNT 12

HANDLE hSema;

DWORD WINAPI WriteDate( LPVOID );

int main( void )
{
	setlocale(0,"rus");
	HANDLE aThread[THREADCOUNT];
	DWORD ThreadID;
	int i;

	hSema = CreateSemaphore( 
		NULL,           
		MAX_FILE_POINTER,  
		MAX_FILE_POINTER,  
		NULL);          

	if (hSema == NULL) 
	{
		printf("CreateSemaphore error: %d\n", GetLastError());
		return 1;
	}

	for( i=0; i < THREADCOUNT; i++ )
	{
		aThread[i] = CreateThread( 
			NULL,      
			0,         
			WriteDate, 
			NULL,       
			0,          
			&ThreadID); 

		if( aThread[i] == NULL )
		{
			printf("CreateThread error: %d\n", GetLastError());
			return 1;
		}
	}

	WaitForMultipleObjects(THREADCOUNT, aThread, TRUE, INFINITE);

	for( i=0; i < THREADCOUNT; i++ )
		CloseHandle(aThread[i]);

	CloseHandle(hSema);

	return 0;
}

DWORD WINAPI WriteDate( LPVOID lpParam )
{
	DWORD dwWaitResult,dwSize; 
	HANDLE hfile;
	wchar_t time_of_work_string[10]; 
	unsigned int time_of_work, start_clock = clock(); 

	while(TRUE)
	{		
		dwWaitResult = WaitForSingleObject(
			hSema,   // handle to semaphore
			3);           // zero-second time-out interval

		switch (dwWaitResult) 
		{ 			
		case WAIT_OBJECT_0: 
			printf("%d entered to critical section\n",GetCurrentThreadId());
			hfile = CreateFile(L"testFile.txt", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 
			time_of_work = clock() - start_clock; 
			//printf("����� ������ ������ %d: (��): %d\n", GetCurrentThreadId(), time_of_work);
			memset(time_of_work_string, 0, sizeof(time_of_work_string)); 
			_itow(time_of_work, time_of_work_string, 10); 
			SetFilePointer(hfile,0,NULL,FILE_END);
			WriteFile(hfile,time_of_work_string,sizeof(time_of_work_string),&dwSize,NULL);
			if (hfile == INVALID_HANDLE_VALUE) 
			{ 
				_putws(TEXT("Error!")); 
			} 
						
			Sleep(5);			
			printf("%d leave to critical section\n",GetCurrentThreadId());
			if (!ReleaseSemaphore( 
				hSema,  
				1,            // increase count by one
				NULL) )       // not interested in previous count
			{
				printf("ReleaseSemaphore error: %d\n", GetLastError());
			}

			//bContinue=FALSE; 			
			return TRUE;
			break; 
		case WAIT_TIMEOUT: 

			printf("Thread %d: wait timed out\n", GetCurrentThreadId());
			break; 
		}
	}
	return TRUE;
}