// sp_lab5.3.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

typedef void (*Algorithm)();

#define THREADCOUNT 3
#define ARRELEMENTS 5000000
#define CRITICAL_MODE 0
#define NONCRITICAL_MODE 1

DWORD WINAPI RunAlgorithm( LPVOID );

void Sum();
void Avg();
void Max();

CRITICAL_SECTION CriticalSection; 
int arr[ARRELEMENTS],mode=-1;

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0,"rus");
	HANDLE aThread[THREADCOUNT];
	DWORD ThreadID;
	unsigned int time_of_work, start_clock; 

	for (int i = 0; i < ARRELEMENTS; i++)
	{
		arr[i]=rand()%100;
	}
	/*----------------------------------------------------------------------------------------*/
	mode=0;
	puts("����� ������ � ������������ ��������:");
	if (!InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x0000001)) 
	{
		puts("�� ������� ������������������� ����������� ������!");
		return 1;
	}
	start_clock = clock(); 
	aThread[0] = CreateThread(NULL, 0,RunAlgorithm,Sum,0,&ThreadID); 
	aThread[1] = CreateThread(NULL, 0,RunAlgorithm,Avg,0,&ThreadID); 
	aThread[2] = CreateThread(NULL, 0,RunAlgorithm,Max,0,&ThreadID); 	
	WaitForMultipleObjects(THREADCOUNT, aThread, TRUE, INFINITE);
	time_of_work=clock()-start_clock;
	printf("����� ����� ����������: %d\n",time_of_work);
	DeleteCriticalSection(&CriticalSection);
	for (int i = 0; i < THREADCOUNT; i++)
	{
		CloseHandle(aThread[i]);
	}
	/*-----------------------------------------------------------------------------------------*/
	mode=1;
	puts("\n����� ������ ��� ����������� ������:");
	start_clock = clock(); 

	aThread[0] = CreateThread(NULL, 0,RunAlgorithm,Sum,0,&ThreadID); 
	aThread[1] = CreateThread(NULL, 0,RunAlgorithm,Avg,0,&ThreadID); 
	aThread[2] = CreateThread(NULL, 0,RunAlgorithm,Max,0,&ThreadID); 
	WaitForMultipleObjects(THREADCOUNT, aThread, TRUE, INFINITE);
	time_of_work=clock()-start_clock;
	printf("����� ����� ����������: %d\n",time_of_work);
	for (int i = 0; i < THREADCOUNT; i++)
	{
		CloseHandle(aThread[i]);
	}
	return 0;
}

DWORD WINAPI RunAlgorithm( LPVOID lpParam )
{	
	Algorithm algoFunction = (Algorithm)lpParam;
	switch (mode)
	{
	case CRITICAL_MODE:
		EnterCriticalSection(&CriticalSection); 
		algoFunction();
		LeaveCriticalSection(&CriticalSection);	
		break;
	case NONCRITICAL_MODE:
		algoFunction();
		break;
	default:
		puts("Error!");
		break;
	}	
	return 0;
}

void Sum()
{
	int sum=0;
	for (int i = 0; i < ARRELEMENTS; i++)
	{
		sum+=arr[i];
	}
	printf("�����: %d\n",sum);
}

void Avg()
{
	double avg=0;
	for (int i = 0; i < ARRELEMENTS; i++)
	{
		avg+=arr[i];
	}
	printf("������� ��������������: %f\n",avg/ARRELEMENTS);
}

void Max()
{
	int max=0;
	for (int i = 0; i < ARRELEMENTS; i++)
	{
		if(max<arr[i])
			max=arr[i];
	}
	printf("������������ �������: %d\n",max);
}
