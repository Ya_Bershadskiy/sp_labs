#include "stdafx.h" 
#define ARRAY_SIZE 5

inline void Sum(unsigned int * arr)
{
	DWORD sum=0;
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		sum+=*(i + arr);
	}
	printf("SUM:%d\n",sum);
	printf("ID: %d\n",GetCurrentThreadId());
	return ;
}

DWORD WINAPI MaxDivider(LPVOID lpParam) 
{ 
	DWORD * arr = (DWORD*)lpParam;
	unsigned int maxDivider[ARRAY_SIZE]; 
	int tmp = 0; 
	DWORD id;
	HANDLE hSon;
	for (int i = 0; i < ARRAY_SIZE; i++) 
	{ 
		for (int div = 100; div > 0; div--) 
		{ 
			if (*(i + arr) % div == 0 && div != *(i + arr)) { 
				maxDivider[tmp] = div; 
				tmp++; 
				break; 
			} 
		} 
	} 
	Sum(maxDivider);
	return 0; 
}

int main() 
{ 
	setlocale(0, "rus"); 
	srand((int)time(NULL)); 

	HANDLE* hThread; 
	DWORD* dwThreadId; 
	int count;
	puts("������� ���������� �������:");
	scanf_s("%d",&count);
	hThread = (HANDLE*)malloc(count*sizeof(HANDLE));
	dwThreadId = (DWORD*)malloc(count*sizeof(DWORD));

	DWORD **arr = (DWORD **)malloc(count * sizeof(DWORD*));
	for(int i = 0; i < count; i++) 
		arr[i] = (DWORD *)malloc(ARRAY_SIZE * sizeof(DWORD));

	for (int i = 0; i < count; i++) 
	{ 
		for (int j = 0; j < ARRAY_SIZE; j++) 
		{ 
			arr[i][j] = 10 + rand() % 90; 
			printf("%d ", arr[i][j]); 
		} 
		puts(""); 
	} 	

	for (int i = 0; i < count; i++) 
	{ 
		hThread[i] = CreateThread(NULL, 0, MaxDivider, arr[i], 0, &dwThreadId[i]);
		WaitForSingleObject(hThread[i],INFINITE);
		if (hThread == NULL) 
		{ 
			puts("CreateThread failed."); 
		} 
		else 
		{ 
			puts("CreateThread SUCCESS."); 
			CloseHandle(hThread); 
		} 
	} 
	return 0; 
}