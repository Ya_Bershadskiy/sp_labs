// sp_lab4.1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#define EXTRACT 1
#define ADD 2
#define PATH2ZIP L"C:\\Program Files\\7-Zip\\7z.exe"
#define BUF_SIZE  255

void printError();
void openZip(wchar_t* params,STARTUPINFO si,PROCESS_INFORMATION pi);

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL,"Rus");	
	int command;
	wchar_t params[BUF_SIZE],archive[BUF_SIZE],outputDir[BUF_SIZE];
	DWORD errCode;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;	
	

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	puts("�������� ��������:\n1-���������� ������ � �����\n2-y������� ������ ����� � ����� �����");
	scanf_s("%d",&command);
	fflush(stdin);
	switch (command)
	{
	case EXTRACT:
		puts("���� � ������:");
		_getws_s(archive);
		puts("���� � �������� ���� ���������:");
		_getws_s(outputDir);
		wcscat(wcscat(wcscat(wcscpy(params,L"7z x "),archive),L" -o"),outputDir);
		openZip(params,si,pi);
		GetExitCodeThread(pi.hThread,&errCode);
		printf("%d",errCode);
		break;
	case ADD:
		puts("��� ������ ������:");
		_getws_s(archive);
		puts("���� � ����� ��� ���������:");
		_getws_s(outputDir);
		wcscat(wcscat(wcscat(wcscpy(params,L"7z a "),archive),L" "),outputDir);
		openZip(params,si,pi);
		break;
	default:
		puts("��� ����� �������");
		break;
	}	
	return 0;
}

void printError()
{
	LPVOID lpMsg;
	DWORD dw;
	dw = GetLastError();
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),
		(LPTSTR)&lpMsg,
		0,
		NULL);
	wprintf((L"%s"),lpMsg);
}

void openZip(wchar_t* params,STARTUPINFO si,PROCESS_INFORMATION pi)
{
	if( !CreateProcess( 
		PATH2ZIP,   
		params,
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,				// No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi )           // Pointer to PROCESS_INFORMATION structure
		) 
	{
		printError();
		return;
	}
	WaitForSingleObject(pi.hProcess,INFINITE);
}